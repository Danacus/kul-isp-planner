import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Paper from '@material-ui/core/Paper';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { h } from 'preact';
import EctsGroup from './ects_group';
import {observer} from 'mobx-react-lite';

const EctsBlock = observer(({ opo_store, block, depth }) => {
    return (
        <Paper elevation={depth}> 
            <Accordion TransitionProps={{ unmountOnExit: true }}>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                >
                    {block.title || ""}
                </AccordionSummary>
                <AccordionDetails>
                    <EctsGroup opo_store={opo_store} group={block.group} depth={depth + 2} /> 
                </AccordionDetails>
            </Accordion>
        </Paper>
    );
});

export default EctsBlock;
