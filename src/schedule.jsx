import { Fragment, h } from 'preact';

export default () => {
    return (
        <div>
            Note: this is only relevant to the Dutch version of the computer science master.<br /><br />
        
            Automatically selecting ical files based on the selected courses in this tool is difficult, due to differences in course IDs within the same course and "ghost courses" (thanks KU Leuven!).<br/><br/>

            You can you this tool made by Arthur instead: <a href="https://principis.be/cw_uurrooster/" target="_blank">https://principis.be/cw_uurrooster/</a>.
        </div>
    );
};
