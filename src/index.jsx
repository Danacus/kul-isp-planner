import "preact/debug";
import { h, render } from 'preact';
import { getApp } from './app';

render(getApp(), document.body);
