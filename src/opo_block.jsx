import { h } from 'preact';

import OpoList from './opo_list';
import {observer} from 'mobx-react-lite';

const OpoBlock = observer(({ opo_store, block, depth }) => (
    <OpoList opo_store={opo_store} opos={block.opos} />
));

export default OpoBlock;
