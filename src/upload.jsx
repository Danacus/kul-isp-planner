import { h } from 'preact';

import {observer} from 'mobx-react-lite';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import DeleteIcon from '@material-ui/icons/Delete';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import { useState } from 'preact/hooks';

const Upload = observer(({ store, profile }) => {
    const opo_store = profile.opo_store;
    
    const [ectsUrl, setEctsUrl] = useState("");

    const handleUpload = e => {
        const fileReader = new FileReader();
        fileReader.readAsText(e.target.files[0], "UTF-8");
        fileReader.onload = e => {
            try {
                opo_store.loadData(JSON.parse(e.target.result));
            } catch (e) {
                console.log("Failed to parse JSON");
                console.error(e);
            }
        };
    };

    const handleUserUpload = e => {
        const fileReader = new FileReader();
        fileReader.readAsText(e.target.files[0], "UTF-8");
        fileReader.onload = e => {
            try {
                opo_store.loadUserData(JSON.parse(e.target.result));
            } catch {
                console.log("Failed to parse JSON");
                console.error(e);
            }
        };
    };

    const handleUserDownload = () => {
        const element = document.createElement("a");
        const file = new Blob([JSON.stringify(opo_store.getUserData())], {type: 'application/json'});
        element.href = URL.createObjectURL(file);
        element.download = "isp-planner-user-data.json";
        document.body.appendChild(element);
        element.click();
    };
    
    const handleDownloadECTS = async () => {
        let url = ectsUrl.replace("https://onderwijsaanbod.kuleuven.be/", "https://kul-ects-scraper.vanoverloop.xyz/api/v1/");
        
        try {
            let response = await fetch(url);
            opo_store.loadData(await response.json());
        } catch {
            console.log("Failed to load ECTS data");
        }
    };

    const handleChangeName = event => {
        profile.setName(event.target.value);
    };

    const CodeView = ({title, code}) => (
        <Accordion TransitionProps={{ unmountOnExit: true }}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
            >
                {title}
            </AccordionSummary>
            <AccordionDetails>
                <Box component="div" my={2} overflow="auto" sx={{ pl: 2 }} style={{ maxHeight: "500px" }}>
                    <pre><code>{code}</code></pre>
                </Box>
            </AccordionDetails>
        </Accordion>
    );

    return (
        <Container>
            <TextField label="Profile name" variant="outlined" value={profile.name} onChange={handleChangeName} /><br /><br />

            ECTS data can be scraped using a tool found <a href="https://gitlab.com/Danacus/kul-ects-scraper">here</a> or some can
            be downloaded from <a href="https://gitlab.com/Danacus/kul-ects-scraper/-/tree/master/data">here</a>. This program uses the JSON files.<br /><br />

            Upload ECTS data:<br />
            <Input type="file" onChange={handleUpload} /><br /><br />
            Or enter ECTS URL:<br />
            <Input onChange={e => setEctsUrl(e.target.value)} /><br />
            <Button variant="outlined" onClick={handleDownloadECTS}>
                Download ECTS data
            </Button><br/><br/>

            Upload user data:<br />
            <Input type="file" onChange={handleUserUpload} /><br /><br />

            <div>
                <Button onClick={handleUserDownload}>Download user data</Button><br/><br/>
                <Button variant="outlined" startIcon={<DeleteIcon />} onClick={() => store.removeProfile()}>
                    Remove profile
                </Button><br/><br/>
                <Button variant="outlined" startIcon={<DeleteIcon />} onClick={() => opo_store.removeData()}>
                    Remove data
                </Button><br/><br/>
                <Button variant="outlined" startIcon={<DeleteIcon />} onClick={() => opo_store.removeUserData()}>
                    Remove user data
                </Button><br/><br/>
                <CodeView title="ECTS Tree" code={JSON.stringify(opo_store.tree, null, 2)} />
                <CodeView title="ECTS OPO List" code={JSON.stringify(opo_store.opos, null, 2)} />
                <CodeView title="User Data" code={JSON.stringify(opo_store.getUserData(), null, 2)} />
            </div>
        </Container>
    );
});

export default Upload;
