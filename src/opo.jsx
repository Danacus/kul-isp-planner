import { useState } from 'preact/hooks';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Rating from '@material-ui/core/Rating';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import SchoolIcon from '@material-ui/icons/School';
import ListItemButton from '@material-ui/core/ListItemButton';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import Slider from '@material-ui/core/Slider';
import Box from '@material-ui/core/Box';
import { Fragment, h } from 'preact';
import {observer} from 'mobx-react-lite';

const Opo = observer(({ index, opo_store, opo_code }) => {
    const [open, setOpen] = useState(false);
    const opo = opo_store.getOpo(opo_code);

    const handleClick = () => {
        setOpen(!open);
    };

    const handleChangeRating = (e, rating) => {
        opo.rate(rating / 5);
    };

    const handleSelect = (e) => {
        let value = e.target.value;
        if (value == null || value === "") {
            opo.select(null);
        } else {
            opo.select(value);
        }
    };

    const handleOpenLink = lang => {
        const url = `https://onderwijsaanbod.kuleuven.be/syllabi/${lang}/${opo.code}${lang.toUpperCase()}.htm`
        window.open(url, '_blank').focus();
    };

    const Phases = ({phases}) => {
        const first = phases.find(phase => phase.phase == 1)
        const second = phases.find(phase => phase.phase == 2)

        return (
            <Fragment>
                <Grid item xs={4} sm={1}>
                    {first && (first.mandatory && "F1: M" || "F1: O") || ""}
                </Grid>
                <Grid item xs={4} sm={1}>
                    {second && (second.mandatory && "F2: M" || "F2: O") || ""}
                </Grid>
            </Fragment>
        );
    };

    const semester = (semester) => {
        switch (semester) {
            case "first": return "< ";
            case "second": return " >";
            case "both": return "<>";
        }
    };

    const options = () => {
        let options = [];

        const add_option = (text, value) => {
            options.push({ key: options.length, text, value });
        };

        const semester = opo.semester;

        for (let phase of opo.phases) {
            let suffix = "";
            
            if (phase.mandatory === false) {
                suffix = " (Optioneel)";
            }

            if (semester == "both") {
                console.log("it is both");
                add_option(`Fase ${phase.phase}${suffix}`, -phase.phase);
            } else {
                console.log("it is not");
                if (semester == "first") {
                    add_option(`Fase ${phase.phase} - Sem 1${suffix}`, 2 * (phase.phase - 1));
                }
                if (semester == "second") {
                    add_option(`Fase ${phase.phase} - Sem 2${suffix}`, 2 * (phase.phase - 1) + 1);
                }
            }
        }

        console.log(options);

        return options;
    };

    const LoadSlider = () => {
        const [value, setValue] = useState(opo.getExamLoad());

        const marks = [
            {
                value: 0,
                label: 'Project',
            },
            {
                value: 50,
                label: '',
            },
            {
                value: 100,
                label: 'Examen',
            },
        ];

        const handleChangeCommited = (event, newValue) => {
            if (typeof newValue === 'number') {
                opo.setLoadBalance(newValue);
            }
        };

        return (
            <Slider
                valueLabelDisplay="auto"
                track={false}
                step={1}
                marks={marks}
                value={value}
                onChange={(_, v) => setValue(v)}
                onChangeCommitted={handleChangeCommited}
            />
        );
    };

    return (
        <Fragment>
            <ListItemButton onClick={handleClick}>
                <ListItemIcon>
                    <SchoolIcon />
                </ListItemIcon>
                <ListItemText primary={opo.name} />
                NL:
                <IconButton onClick={() => handleOpenLink('n')}>
                    <OpenInNewIcon />
                </IconButton>
                EN:
                <IconButton onClick={() => handleOpenLink('e')}>
                    <OpenInNewIcon />
                </IconButton>
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={open} timeout="auto" orientation="vertical" unmountOnExit={true}>
                <Grid container component="div" sx={{ pl: 6, pr: 3 }} spacing={2}>
                    <Grid item xs={6} sm={1}>
                        {opo.sp} sp.
                    </Grid>
                    <Grid item xs={6} sm={2}>
                        {opo.code}
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        {opo.docents.join(", ")}
                    </Grid>
                    <Grid item xs={4} sm={2}>
                        {semester(opo.semester)}
                    </Grid>
                    <Phases phases={opo.phases} />
                    <Grid item xs={12} sm={3}>
                        <Rating value={opo.rating() * 5} precision={0.5} onChange={handleChangeRating} />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <FormControl fullWidth>
                            <InputLabel id={opo.code + "_select_label"}>Semester</InputLabel>
                            <Select
                                labelId={opo.code + "_select_label"}
                                value={opo.selection() == undefined ? "" : opo.selection()}
                                label="Semester"
                                onChange={handleSelect}
                            >
                                <MenuItem value="">
                                    None
                                </MenuItem>
                                {options().map(option => (
                                    <MenuItem value={option.value}>{option.text}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <Box sx={{ pl: 4 }}>
                            <LoadSlider />
                        </Box>
                    </Grid>
                </Grid>
            </Collapse>
        </Fragment>
    );
});

export default Opo;
