import List from '@material-ui/core/List';
import { h } from 'preact';

import Opo from './opo';
import {observer} from 'mobx-react-lite';

const OpoList = observer(({ opo_store, opos }) => (
    <List
      sx={{ width: '100%' }}
    >
        {opos.map((opo, index) => 
            <Opo opo_store={opo_store} opo_code={opo} index={index} />
        )}
    </List>
));

export default OpoList;
