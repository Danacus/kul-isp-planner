import Grid from '@material-ui/core/Grid';
import { h } from 'preact';
import EctsGroup from './ects_group';
import Planning from './planning';
import {observer} from 'mobx-react-lite';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const Plan = observer(({ profile }) => {
    const wide_screen = useMediaQuery('(min-width:600px)');

    if (wide_screen) {
        return (
            <Grid container spacing={2}>
                <Grid item xs={6}>
                    <EctsGroup opo_store={profile.opo_store} group={profile.opo_store.tree} depth={0} />
                </Grid>
                <Grid item xs={6}>
                    <Planning profile={profile} />
                </Grid>
            </Grid>
        );
    } else {
        return (
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Planning profile={profile} />
                </Grid>
                <Grid item xs={12}>
                    <EctsGroup opo_store={profile.opo_store} group={profile.opo_store.tree} depth={0} />
                </Grid>
            </Grid>
        );
    }
});

export default Plan;
