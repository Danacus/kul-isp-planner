import {autorun, makeAutoObservable} from 'mobx';

const localStorageGet = key => {
	try {
		return localStorage.getItem(key);
	} catch (e) {
		return null;
	}
};

const localStorageSet = (key, value) => {
	try {
		localStorage.setItem(key, value);
	} catch (e) {}
};

const saveState = (state) => {
	localStorageSet('state', JSON.stringify(state));
}

const getSavedState = () => {
	let state;
	try {
		state = JSON.parse(localStorageGet('state'));
	} catch (e) {}
	return state || {};
}

class Opo {
    constructor(config, obj) {
        this.config = config || {};
        Object.assign(this, obj);
        makeAutoObservable(this);
    }

    select(selection) {
        this.config.selection = selection;
    }

    rate(rating) {
        this.config.rating = rating;
    }

    selection() {
        return this.config.selection;
    }

    rating() {
        return this.config.rating;
    }

    inSemester(semester) {
        if (this.config.selection < 0) {
            // Deal with courses that fill an entire year
            return semester === -(2 * (this.config.selection + 1)) || semester === -(2 * (this.config.selection + 1)) + 1;
        } else {
            return this.config.selection == semester;
        }
    }

    setLoadBalance(load) {
        this.config.load_bal = load;
    }

    getExamLoad() {
        if (typeof this.config.load_bal === "number") {
            return this.config.load_bal;
        } else {
            return 50;
        }
    }

    getProjectLoad() {
        return 100 - this.getExamLoad();
    }

    toJSON() {
        let res = Object.assign({}, { ...this });
        delete res.config;
        return res;
    }
}

class OpoStore {
    tree;
    opos;

    constructor(obj) {
        this.loadData(obj);
        makeAutoObservable(this);
    }

    isReady() {
        return this.tree != null && this.opos != null;
    }

    loadData(obj) {
        let userData = this.getUserData();

        if (obj) {
            this.tree = obj.tree || null;
            if (obj && obj.opos) {
                this.opos = {};
                for (const [code, opo] of Object.entries(obj.opos)) {
                    const config = obj.opo_config && obj.opo_config[code] || undefined;
                    this.opos[code] = new Opo(config, opo);
                }

                if (userData) {
                    this.loadUserData(userData);
                }
            } else {
                this.opos = null;
            }
        }
    }

    loadUserData(data) {
        for (const [code, config] of Object.entries(data)) {
            let opo = this.opos[code];
            if (opo) {
                opo.config = config;
            }
        }
    }

    getOpo(code) {
        return this.opos[code];
    }

    getOpoConfig(code) {
        return this.opos[code].config;
    }

    getUserData() {
        let opo_config = {};

        if (this.opos) {
            for (const opo of Object.values(this.opos)) {
                if (opo.config != null && Object.keys(opo.config).length > 0) {
                    opo_config[opo.code] = opo.config;
                }
            }
        }

        return opo_config;
    }

    selectedOpos(semester) {
        let res = [];

        for (const [code, opo] of Object.entries(this.opos)) {
            if (opo && opo.config && opo.config.selection != undefined) {
                if (semester == undefined || semester == null || opo.inSemester(semester)) {
                    res.push(opo);
                }
            }
        }

        return res;
    }

    removeData() {
        this.tree = null;
        this.opos = null;
    }

    removeUserData() {
        if (this.opos) {
            for (let opo of Object.values(this.opos)) {
                delete opo.config;
            }
        }
    }

    toJSON() {
        let res = Object.assign({}, { ...this });
        res.opo_config = this.getUserData();
        return res;
    }
}

class Profile {
    num_semesters;
    opo_store;
    name;

    constructor(obj) {
        this.num_semesters = obj.num_semesters || 4;
        this.opo_store = new OpoStore(obj.opo_store);
        this.name = obj.name || "New Profile";
        makeAutoObservable(this);
    }

    setName(name) {
        this.name = name;
    }
}

class Store {
    page;
    data;
    profiles;
    active_profile;

    constructor(obj) {
        this.page = obj.page || 0;
        this.data = obj.data;
        this.profiles = obj.profiles && obj.profiles.map(p => new Profile(p)) || [];
        this.active_profile = obj.active_profile || 0;

        if (this.profiles.length === 0) {
            this.page = 0;
        } 

        makeAutoObservable(this);
    }

    newProfile() {
        this.profiles.push(new Profile({}));
        this.active_profile = this.profiles.length - 1;
        this.page = 1;
    }

    setProfile(index) {
        if (index >= this.profiles.length) {
            index = this.profiles.length - 1;
        }

        this.active_profile = index;
    }

    getProfile() {
        if (this.active_profile >= this.profiles.length) {
            this.active_profile = 0;
        }

        return this.profiles[this.active_profile];
    }

    setPage(page) {
        this.page = page; 
    }

    removeProfile() {
        this.profiles.splice(this.active_profile--, 1);
        if (this.active_profile < 0) {
            this.active_profile = 0;
            this.setPage(0);
        }
    }
}

const newStore = () => {
    let storedState = getSavedState() || {};
	  let store = new Store(storedState);
    return store;
}

export const store = newStore();

autorun(() => saveState(store));
