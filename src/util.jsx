import { useCallback, useState } from 'preact/hooks';

export const useToggleBool = () => {
    const [active, setActive] = useState(true);
    const toggle = useCallback(() => {
        setActive(!active);
    }, [active]);
    return { active, toggle };
};
