import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Rating from '@material-ui/core/Rating';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Slider from '@material-ui/core/Slider';
import Box from '@material-ui/core/Box';
import Input from '@material-ui/core/Input';
import { Fragment, h } from 'preact';
import OpoList from './opo_list';
import {observer} from 'mobx-react-lite';

const Planning = observer(({ profile }) => {
    const { opo_store, num_semesters } = profile;

    const marks = [
        {
            value: 0,
            label: 'Project',
        },
        {
            value: 50,
            label: '',
        },
        {
            value: 100,
            label: 'Examen',
        },
    ];

    const semesterTitle = i => {
        return `Fase ${Math.floor(i / 2) + 1}: Semester ${(i % 2) + 1}`;
    };

    const getRating = opos => {
        let sum = opos.map(opo => opo.rating() || 0.5).reduce((acc, value) => acc + value, 0);
        return sum / opos.length;
    };

    const getLoadBalance = opos => {
        let totalSP = getSP(opos);
        let sum = opos.map(opo => opo.getExamLoad() * (mapSP(opo) / totalSP)).reduce((acc, value) => acc + value, 0);
        return sum;
    };

    const mapSP = (opo, split) => {
        if (split && opo.selection() < 0) {
            // Takes entire year, split SP in half
            return opo.sp / 2;
        } else {
            return opo.sp;
        }
    };

    const getSP = (opos, split) => {
        return opos.map(opo => mapSP(opo, split)).reduce((acc, value) => acc + value, 0);
    };

    const allOpos = () => opo_store.selectedOpos();

    return (
        <Paper>
            <Typography sx={{ pl: 2, pt: 2, pb: 2 }}>
                Number of semesters: <Input type="number" value={profile.num_semesters} onChange={e => profile.num_semesters = parseInt(e.target.value)} /><br/>
                Average rating: <Rating value={getRating(allOpos()) * 5} precision={0.5} readOnly /><br/>
                Average load: <br />
                <Box sx={{ width: 0.3, pl: 4 }}>
                    <Slider
                        valueLabelDisplay="auto"
                        track={false}
                        step={1}
                        marks={marks}
                        value={getLoadBalance(allOpos())}
                        disabled
                    />
                </Box><br />
                Total SP: {getSP(allOpos(), false)} / {num_semesters * 30}
            </Typography>
            {[...Array(num_semesters).keys()].map(i => (
                <Accordion  TransitionProps={{ unmountOnExit: true }}>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                    >
                        {semesterTitle(i)}
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography sx={{ pl: 1, pb: 1 }}>
                            Average rating: <Rating value={getRating(opo_store.selectedOpos(i)) * 5} precision={0.5} readOnly /><br/>
                            Average load: <br />
                            <Box sx={{ width: 0.3, pl: 4 }}>
                                <Slider
                                    valueLabelDisplay="auto"
                                    track={false}
                                    step={1}
                                    marks={marks}
                                    value={getLoadBalance(opo_store.selectedOpos(i))}
                                    disabled
                                />
                            </Box><br />
                            Total SP: {getSP(opo_store.selectedOpos(i), true)} / 30
                        </Typography>
                        <OpoList opo_store={opo_store} opos={opo_store.selectedOpos(i).map(opo => opo.code)} />
                    </AccordionDetails>
                </Accordion>
            ))}
        </Paper>
    );
});

export default Planning;
