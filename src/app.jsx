import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { Fragment, h } from 'preact';

import Grid from '@material-ui/core/Grid';

import { store } from './store';
import Upload from './upload';
import Plan from './plan';
import Schedule from './schedule';
import {observer} from 'mobx-react-lite';
import ThemeProvider from '@material-ui/core/styles/ThemeProvider';
import { createTheme } from '@material-ui/core/styles';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import CssBaseline from '@material-ui/core/CssBaseline';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import HomeIcon from '@material-ui/icons/Home';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import AddIcon from '@material-ui/icons/Add';

const theme = createTheme({
  palette: {
    mode: 'dark',
  },
});

const Home = observer(({ store }) => {
    const handleClick = () => {
        if (store.profiles.length == 0) {
            store.newProfile();
        }
        store.setPage(1);
    };

    return (
        <Container>
            Welcome to the experimental ISP planner.<br /><br />
            Disclaimer: This website is no affiliated with KU Leuven. I am not responsible for the correctness of the information provided by this program.
            All data is stored locally in browser storage and user data can be exported/imported at any time. It is recommended that you export data
            regularly as the program may have bugs and it may or may not remove all stored data.<br /><br />

            This is free software licensed under GNU Affero General Public License. Source code can be found <a href="https://gitlab.com/Danacus/kul-isp-planner">here</a>.<br /><br />

            <Button onClick={handleClick}>
                Continue
            </Button>
        </Container>
    );
});

const AppTabs = observer(({ store }) => {
    const a11yProps = index => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    }

    const handleChange = (event, newValue) => {
        store.setPage(newValue + 1);
    };

    return (
        <Box sx={{ width: '100%' }}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={store.page - 1} onChange={handleChange} aria-label="pages">
              <Tab label="Manage profile" {...a11yProps(0)} />
              <Tab label="Planning" {...a11yProps(1)} disabled={!(store.getProfile() != null && store.getProfile().opo_store.isReady())} />
              <Tab label="Schedule" {...a11yProps(2)} disabled={!(store.getProfile() != null && store.getProfile().opo_store.isReady())} />
            </Tabs>
          </Box>
        </Box>
    );
});

const ProfileSelect = observer(({ store }) => {
    const handleChange = (e) => {
        let value = e.target.value;
        store.setProfile(value);
    };

    return (
        <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Profile</InputLabel>
            <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={store.active_profile}
                label="Profile"
                onChange={handleChange}
            >
                {store.profiles.map((profile, index) => (
                    <MenuItem value={index}>{profile.name}</MenuItem>
                ))}
            </Select>
        </FormControl>
    );
});

const Page = observer(({ store }) => {
    switch (store.page) {
        case 0: return <Home store={store} />;
        case 1: return <Upload store={store} profile={store.getProfile()} />;
        case 2: return <Plan profile={store.getProfile()} />;
        case 3: return <Schedule profile={store.getProfile()} />;
    }
});

const App = observer(({ store }) => (
    <Grid container sx={{ pl: 2, pt: 2, pr: 2, pb: 2 }} spacing={2}>
        {store.page !== 0 &&
        <Fragment>
            <Grid item xs={1}>
                <IconButton onClick={() => store.setPage(0)}>
                    <HomeIcon />
                </IconButton>
            </Grid>
            <Grid item xs={8}>
                <AppTabs store={store} />
            </Grid>
            <Grid item xs={2}>
                <ProfileSelect store={store} />
            </Grid>
            <Grid item xs={1}>
                <IconButton onClick={() => store.newProfile()}>
                    <AddIcon />
                </IconButton>
            </Grid>
        </Fragment>
        }
        <Grid item xs={12}>
            <Page store={store} />
        </Grid>
    </Grid>
));

export const getApp = () => (
    <ThemeProvider theme={theme}>
        <CssBaseline />
        <App store={store} />
    </ThemeProvider>
)
