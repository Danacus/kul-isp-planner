import { h, Fragment } from 'preact';
import EctsBlock from './ects_block';
import OpoBlock from './opo_block';
import {observer} from 'mobx-react-lite';

const EctsGroup = observer(({ opo_store, group, depth }) => {
    return (
        <Fragment>
            {group.choice_rules && group.choice_rules !== "None" && <p>{group.choice_rules}</p>}
            {group.blocks.map(block =>
                block.group !== undefined &&
                <EctsBlock opo_store={opo_store} block={block} depth={depth} /> ||
                <OpoBlock opo_store={opo_store} block={block} depth={depth} />
            )}
        </Fragment>
    );
});

export default EctsGroup;
